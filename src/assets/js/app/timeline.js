$(document).ready(function () {
  /**
   * Plugin for linking multiple owl instances
   * @version 1.0.0
   * @author David Deutsch
   * @license The MIT License (MIT)
   */
  (function ($, window, document, undefined) {
    /**
     * Creates the Linked plugin.
     * @class The Linked Plugin
     * @param {Owl} carousel - The Owl Carousel
     */
    var Linked = function (carousel) {
      /**
       * Reference to the core.
       * @protected
       * @type {Owl}
       */
      this._core = carousel;
      /**
       * All event handlers.
       * @protected
       * @type {Object}
       */
      this._handlers = {
        "dragged.owl.carousel changed.owl.carousel": $.proxy(function (e) {
          if (e.namespace && this._core.settings.linked) {
            this.update(e);
          }
        }, this),
        "linked.to.owl.carousel": $.proxy(function (
          e,
          index,
          speed,
          standard,
          propagate
        ) {
          if (e.namespace && this._core.settings.linked) {
            this.toSlide(index, speed, propagate);
          }
        },
        this),
      };
      // register event handlers
      this._core.$element.on(this._handlers);
      // set default options
      this._core.options = $.extend({}, Linked.Defaults, this._core.options);
    };
    /**
     * Default options.
     * @public
     */
    Linked.Defaults = {
      linked: false,
    };
    /**
     * Updated linked instances
     */
    Linked.prototype.update = function (e) {
      this.toSlide(e.relatedTarget.relative(e.item.index));
    };
    /**
     * Carry out the to.owl.carousel proxy function
     * @param {int} index
     * @param {int} speed
     * @param {bool} propagate
     */
    Linked.prototype.toSlide = function (index, speed, propagate) {
      var id = this._core.$element.attr("id"),
        linked = this._core.settings.linked.split(",");
      if (typeof propagate == "undefined") {
        propagate = true;
      }
      index = index || 0;
      if (propagate) {
        $.each(linked, function (i, el) {
          $(el).trigger("linked.to.owl.carousel", [index, 300, true, false]);
        });
      } else {
        this._core.$element.trigger("to.owl.carousel", [index, 300, true]);
        if (this._core.settings.current) {
          this._core._plugins.current.switchTo(index);
        }
      }
    };
    /**
     * Destroys the plugin.
     * @protected
     */
    Linked.prototype.destroy = function () {
      var handler, property;
      for (handler in this._handlers) {
        this.$element.off(handler, this._handlers[handler]);
      }
      for (property in Object.getOwnPropertyNames(this)) {
        typeof this[property] != "function" && (this[property] = null);
      }
    };
    $.fn.owlCarousel.Constructor.Plugins.linked = Linked;
  })(window.Zepto || window.jQuery, window, document);

  var milestoneCarousel = $(".milestone-carousel").owlCarousel({
    items: 1,
    loop: false,
    center: true,
    margin: 0,
    stageClass: "owl-staging",
    mouseDrag: true,
    touchDrag: true,
    //URLhashListener:true,
    //startPosition: 'URLHash',
    responsiveClass: true,
    linked: ".years-carousel",
    onInitialized: function (event) {
      let element = jQuery(event.target);
      element.parents(".decades").addClass("first_one");
      var bg_image = element.find(".milestone").attr("data-value");
      var bg_image_mobile = element
        .find(".milestone")
        .attr("data-value-mobile");
      if (window.screen.width < 580) {
        $(".milestone-carousel").css(
          "background-image",
          "url('" + bg_image_mobile + "')"
        );
      } else {
        $(".milestone-carousel").css(
          "background-image",
          "url('" + bg_image + "')"
        );
      }
    },
  });

  var current = Number(
    $(".years-carousel .parent-owl-item:first-child > .years > a").text()
  );
  var currentDecade = Number(
    $(".years-carousel .parent-owl-item:first-child > .years > a").text()
  );
  setTimeout(() => {
    current = Number(
      $(".years-carousel .parent-owl-item:first-child > .years > a").text()
    );
    currentDecade = Number(
      $(".years-carousel .parent-owl-item:first-child > .years > a").text()
    );
    // console.log(current);
  }, 1000);
  if ($(window).width() <= 580) {
    milestoneCarousel.on("changed.owl.carousel", function (event) {
      function getValue(v) {
        if (v % 10 == 0) {
          if (v > current) {
            $(".years-carousel").trigger(
              "to.owl.carousel",
              $(".years-carousel .parent-owl-item.end").index()
            );
            currentDecade = currentDecade + 10;
          }
        }
      }
      setTimeout(() => {
        var chn = Number(
          $(".years-carousel .parent-owl-item.end .years > .active_year").text()
        );
        // console.log(chn);
        if (chn < currentDecade) {
          $(".years-carousel").trigger(
            "to.owl.carousel",
            $(".years-carousel .parent-owl-item.end").index()
          );
          currentDecade = currentDecade - 10;
          // console.log("<-prev");
        } else {
          // console.log("up->");
          getValue(chn);
        }

        current = chn;
        // console.log("Current " , current);
        // console.log("Current Decade" , currentDecade);
      }, 300);
    });
  }

  $(".years-carousel").owlCarousel({
    nav: true,
    loop: false,
    dots: false,
    responsiveClass: true,
    items: 7,
    margin: 0,
    // center:true,
    mouseDrag: true,
    touchDrag: true,
    itemClass: "owl-item parent-owl-item",
    responsive: {
      0: {
        items: 1,
        nav: false,
      },
      768: {
        items: 7,
        nav: false,
      },
    },
    onInitialized: function (event) {
      let element = jQuery(event.target);
      element.find(".parent-owl-item").addClass("end");
      $("body").addClass("timeline_hide_header");
      // setTimeout(function () {
      //   $('html, body').animate({
      //     scrollTop: $(".decades").offset().top
      //   }, 1000);
      // }, 1000);
    },
    // linked: ".milestone-carousel"
  });

  // var milestoneowl = $(".milestone-carousel");
  // milestoneowl.on("mousewheel", ".owl-stage", function (e) {
  //   if (e.originalEvent.wheelDelta > 0) {
  //     milestoneowl.trigger("next.owl");
  //   } else {
  //     milestoneowl.trigger("prev.owl");
  //   }
  //   e.preventDefault();
  // });

  var milestoneowl = $(".milestone-carousel");
  var scrollCount = null;
  var scroll = null;
  // milestoneowl.on('wheel mousewheel DOMMouseScroll', (function(e) {
  //   e.preventDefault();
  //   clearTimeout(scroll);
  //   scroll = setTimeout(function() {
  //     scrollCount = 0;
  //   }, 200);
  //   if (scrollCount) return 0;
  //   scrollCount = 1;
  //   if (e.originalEvent.deltaY < 0) {
  //     $(this).trigger("prev.owl");
  //   } else {
  //     $(this).trigger("next.owl");
  //   }
  // }));

  $(".milestone-carousel.owl-carousel").on(
    "changed.owl.carousel initialized.owl.carousel",
    function (event) {
      if ($(".decade_inner").parent().hasClass("end")) {
        $(".decades").addClass("gradient_banner");
      } else {
        $(".decades").removeClass("gradient_banner");
      }
      if ($(".decade_other").parent().hasClass("end")) {
        $(".decades").addClass("black_gradient_banner");
      } else {
        $(".decades").removeClass("black_gradient_banner");
      }
      var bg_image = $(".owl-item.end").find(".milestone").attr("data-value");
      var bg_image_mobile = $(".owl-item.end").find(".milestone").attr("data-value-mobile");



      if (window.screen.width < 580) {
        $(".milestone-carousel").css(
          "background-image",
          "url('" + bg_image_mobile + "')"
        );
      } else {
        $(".milestone-carousel").css(
          "background-image",
          "url('" + bg_image + "')"
        );
      }
    }
  );

  $(".decades .custom-owl-right-arow, .scroll").click(function () {
    $(this)
      .parents("section")
      .find(".milestone-carousel")
      .trigger("next.owl.carousel");
  });

  $(".decades .custom-owl-left-arow").click(function () {
    $(this)
      .parents("section")
      .find(".milestone-carousel")
      .trigger("prev.owl.carousel");
  });

  $(".owl-carousel:not(.years-carousel)").on(
    "initialized.owl.carousel changed.owl.carousel refreshed.owl.carousel",
    function (e) {
      if (!e.namespace) return;
      var carousel = e.relatedTarget,
        current = carousel.current();
      if (current === carousel.maximum() && current != "0") {
        $(this).parents("section").addClass("last_one");
        $("body").addClass("timeline_show_footer");
      } else {
        $(this).parents("section").removeClass("last_one");
        $("body").removeClass("timeline_show_footer");
      }
      if (current === carousel.minimum()) {
        $(this).parents("section").addClass("first_one");
      } else {
        $(this).parents("section").removeClass("first_one");
      }
    }
  );

  $(".nesteditem").owlCarousel({
    items: 10,
    //center: true,
    itemClass: "owl-item nested-owl-item",
    //linked: ".milestone-carousel",
    onInitialized: function (event) {
      let element = jQuery(event.target);
      element.find(".nested-owl-item").removeClass("end");
      $(".nesteditem").addClass("inactive");
    },
  });

  $(".milestone-carousel").on(
    "changed.owl.carousel initialized.owl.carousel",
    function (event) {
      setTimeout(function () {
        $("body").removeClass("timeline_hide_header");
      }, 1000);
      $(".years-carousel a").each(function () {
        var active_hash = $(this).attr("href");
        var current = $('[href*="' + active_hash + '"]');
        if (window.location.hash === active_hash) {
          $(current).parents(".owl-item").addClass("end");
          $(current).parents(".owl-item").siblings().removeClass("end");
          $(this).addClass("active_year");
        } else {
          $(current).parents(".nested-owl-item").removeClass("end");
          $(this).removeClass("active_year");
        }
        if (current.hasClass("active_year")) {
          $(this).parents(".nesteditem").removeClass("inactive");
        }
      });
    }
  );
});
$(document).ready(function () {
  if ($(".milestone-carousel").length > 0) {
    window.location.hash = "";
  }
});
