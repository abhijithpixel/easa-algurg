// our expertise

// const { Alert } = require("bootstrap");

// const mouse = new smoothMouse();

// function smoothMouse() {
// 	const target = (document.documentElement || document.body.parentNode || document.body)
//   const speed = 100;
//   const smooth = 25;

// 	let moving = false;
// 	let pos = target.scrollTop;

// 	target.addEventListener('mousewheel', scroll, { passive: false });

// 	function scroll(e) {
//     // disable default scrolling
//     e.preventDefault();

//     let delta;

//     if (e.detail) {
// 			if (e.wheelDelta) delta = e.wheelDelta / e.detail / 40 * (e.detail > 0 ? 1 : -1);
//       else delta = -e.detail / 3;
// 		} else {
// 			delta = e.wheelDelta / 120;
//     }

// 		pos += -delta * speed;
// 		pos = Math.max(0, Math.min(pos, target.scrollHeight - target.clientHeight));

// 		if (!moving) update();
// 	}

// 	function update() {
// 		moving = true;

// 		const delta = (pos - target.scrollTop) / smooth;

// 		target.scrollTop += delta;

// 		if (Math.abs(delta) > 0.05) window.requestAnimationFrame(update);
// 		else moving = false;
// 	}
// }


  $(window).scroll(function() {
    $("a[data-scroll]").click((function(e) {
      e.preventDefault();
      var target = "#" + $(this).data("scroll"),
          $target = $(target);
      $("html, body").stop().animate({
          scrollTop: $target.offset().top
      }, 800, "swing")
  }))
 

});



$('.featured-galery-box').on('initialized.owl.carousel changed.owl.carousel', function(e) {
  if (!e.namespace)  {
    return;
  }
  
  $(this).closest('section').find('p.slider-counter').text(e.relatedTarget.relative(e.relatedTarget.current()) + 1 + '/' + '0' + e.relatedTarget.items().length);
}).owlCarousel({
    autoplay: false,
    lazyLoad: true,
    loop: false,
    margin: 230,
    items:1.5,    
    autoHeight: true,
    autoWidth:true,
    autoplayTimeout: 7000,
    smartSpeed: 1500,
    nav: false,   
    responsiveClass:true,
      //navText : ["<i class='fa fa-chevron-left'><svg xmlns='http://www.w3.org/2000/svg' width='30.525' height='16.017' viewBox='0 0 30.525 16.017' >    <path id='arow' d='M-22.662.133A.5.5,0,0,0-23.3.1l-.067.061-7.023,7.517a.5.5,0,0,0-.058.6l.058.076,7.023,7.5a.5.5,0,0,0,.707.027.5.5,0,0,0,.082-.635l-.055-.071L-28.891,8.5h28.4S0,8.442,0,8.008s-.488-.5-.488-.5H-28.875l6.24-6.67A.5.5,0,0,0-22.6.2Z' transform='translate(30.525)' fill='#84754e'></path>  </svg></i>","<i class='fa fa-chevron-right'><svg xmlns='http://www.w3.org/2000/svg' width='30.525' height='16.017' viewBox='0 0 30.525 16.017'>  <path id='arow' d='M22.662.133A.5.5,0,0,1,23.3.1l.067.061,7.023,7.517a.5.5,0,0,1,.058.6l-.058.076-7.023,7.5a.5.5,0,0,1-.789-.608l.055-.071L28.891,8.5H.488S0,8.442,0,8.008s.488-.5.488-.5H28.875L22.635.839A.5.5,0,0,1,22.6.2Z' fill='#84754e'></path></svg></i>"],
    responsive:{
      0:{
        items:1,
        margin:0,   
        autoWidth:false,  
        autoHeight: false,      
      },
      768:{
        items:1,
        margin:100           
      },
      992:{
        item:1.5,  
        margin: 100,           
      },  
      1200:{
        item:1.5,  
        margin: 230,           
      },       
    }             
});


$('.glry-next').click(function() {
  $(this).closest('section').find(".featured-galery-box").trigger('next.owl.carousel');
});
$('.glry-prev').click(function() {
  // alert('done');/
  $(this).closest('section').find(".featured-galery-box").trigger('prev.owl.carousel');
});

$('.featured-galery-box-two').on('initialized.owl.carousel changed.owl.carousel', function(e) {
  if (!e.namespace)  {
    return;
  }
  $(this).closest('section').find('p.slider-counter').text(e.relatedTarget.relative(e.relatedTarget.current()) + 2 + '/' + '0' + e.relatedTarget.items().length);
}).owlCarousel({
  autoplay: false,
  lazyLoad: true,
  loop: false,          
  margin:40, 
  items:1,     
  autoplayTimeout: 7000,
  smartSpeed: 800,
  nav: false,
  responsive:{
    992:{
      items:2,         
    },       
  }             
});
$('.glry-next').click(function() {
  $(this).closest('section').find(".featured-galery-box-two").trigger('next.owl.carousel');
});
$('.glry-prev').click(function() {
  $(this).closest('section').find(".featured-galery-box-two").trigger('prev.owl.carousel');
});

//dream
$(".dream").owlCarousel({
  autoplay: true,
  lazyLoad: true,
  loop: true,      
  items:1,    
  // margin:40,      
  autoplayTimeout: 7000,
  smartSpeed: 800,
  nav: false,  
});
$('.dream-left').click(function() {
  $(".dream").trigger('next.owl.carousel');
});
$(' .dream-right').click(function() {
  $(".dream ").trigger('prev.owl.carousel');
});


//solutions

$(".solution-bg").owlCarousel({
  autoplay: true,
  lazyLoad: true,
  loop: true,      
  items:1,    
  // margin:40,      
  autoplayHoverPause:true,
  autoplayTimeout: 7000,
  smartSpeed: 1000,
  nav: false,  
});

$('.right-arow').click(function() {
  $(this).closest('section').find(".solution-bg").trigger('next.owl.carousel');
});
$('.left-arow').click(function() {
  $(this).closest('section').find(".solution-bg").trigger('prev.owl.carousel');
});

$('.custom-owl-right-arow').click(function() {
  $(this).parents('section').find(".featured-galery-box").trigger('next.owl.carousel');
});
$('.custom-owl-left-arow').click(function() {
  $(this).parents('section').find(".featured-galery-box").trigger('prev.owl.carousel');
});

// news

$('.newses').owlCarousel({
  autoplay: false,
  lazyLoad: true,
  loop: false,    
  items:2,
  margin: 60,
  // responsiveClass: true,

  autoplayTimeout: 7000,
  smartSpeed: 800,
  nav: false,          
  dots: false,
  responsiveClass:true,
  responsive:{
    0:{
      items:1,
      margin:30           
    },
    768:{
      items:2,                
    },
   
         
  }

});

$('.all-next').click(function() {
  $(".newses").trigger('next.owl.carousel');
});
$('.all-prev').click(function() {
  $(".newses").trigger('prev.owl.carousel');
});

//commitment

$('.service').owlCarousel({
  autoplay: false,
  lazyLoad: true,
  loop: true,    
  items:2,
  margin: 60,
  autoplayTimeout: 7000,
  smartSpeed: 800,
  nav: true,          
  dots: true,
  responsiveClass:true,
  responsive:{
    0:{
      items:1,
      margin:30           
    },
    768:{
      items:2,                
    },
   
         
  }

});
$('.single_service').owlCarousel({
  autoplay: false,
  lazyLoad: true,
  loop: true,    
  items:1,
  margin: 60,
  autoplayTimeout: 7000,
  smartSpeed: 800,
  nav: true,          
  dots: true,
  responsiveClass:true,
  responsive:{        
  }

});
$('.our-next').click(function() {
  $(this).closest('section').find(".owl-carousel").trigger('next.owl.carousel');
});
$('.our-prev').click(function() {
  $(this).closest('section').find(".owl-carousel").trigger('prev.owl.carousel');
});

$('.twt-feeds').owlCarousel({
  autoplay: false,
  lazyLoad: true,
  loop: true,    
  item:3,
  margin: 40,

  autoHeight: true,
  autoWidth:false,
  autoplayTimeout: 7000,
  smartSpeed: 800,
  nav: false,          
  dots:false,
  responsiveClass:true,
    responsive:{
      0:{
        items:1,
        margin:60           
      },
      768:{
        items:2,
        margin:40           
      },
      992:{
        item:3,  
                  
      },  
           
    }

});
$('.be-next').click(function() {
  $(".twt-feeds").trigger('next.owl.carousel');
});
$('.be-prev').click(function() {
  $(".twt-feeds").trigger('prev.owl.carousel');
});

$(window).scroll(function(){
  if ($(this).scrollTop() > 500) {
     $('header').addClass('fix-nav');
  } else {
     $('header').removeClass('fix-nav');
  }
});


// $('.service-hide .show').click(function() {
//   $(this).toggleClass('open');
//   $('ul.tab-list').toggleClass('open');
// });
// $('ul.tab-list li').click(function() {
//   $(this).toggleClass('selected');
// });

$('.tab-list .toggle_tab').click(function() {
  $(this).toggleClass('open');
  $(this).parent('.tab-list').toggleClass('open');
  var $el = $(this).find('.toggle_tab_text');
  $el.text($el.text() == "SHOW MORE" ? "SHOW LESS": "SHOW MORE");
});




//mennu

$(".mobile-nav").click(function(){
  $(this).toggleClass("open");
  $('.top-nav ul').toggleClass('open-nav');
});


$(".top-search").click(function(){  
  $('.top-search-box').toggleClass('srch-open');
});
$(".close").click(function(){  
  $('.top-search-box').removeClass('srch-open');
});

//intersection

// const statusBox = document.querySelector('.status');
// let intersectItem = document.querySelectorAll('.item'), i;
// let visiblity = 'invisible';
// let statusText = 'Nothing is visible, scroll!';

// const iObserver = new IntersectionObserver(items => {

//   // Intersecting? (is the element in the available viewport)
//   if (items[0].isIntersecting) {
		
// 		// Log visibility
//     console.log('Visible? Yes');

//     // Time when intersect occured (relative to page load)
//     console.log('When? After ' + milliToSeconds(items[0].time));

//     // Position values
//     console.log('Position:')
//     console.log(items[0].boundingClientRect);
		
// 		// Element that is visible
// 		console.log(items[0].target);
		
// 		// Update status vars
// 		visiblity = 'visible';
//     statusText = items[0].target.textContent + ' is visible';

//   } else {
//     console.log('Visible? No')
//     visiblity = 'invisible';
//     statusText = 'Nothing is visible, scroll!';
//   }

//   console.log('---------------');

//   // Update status
//   updateStatus(statusText);
// });

// // Loop through and observe intersect items
// for (i = 0; i < intersectItem.length; ++i) {
//   iObserver.observe(intersectItem[i]);
// }

// // Update visibility status
// function updateStatus(statusText) {
//   statusBox.innerHTML = statusText;
//   statusBox.className = 'status status--' + visiblity;
// }

// // Milliseconds -> Seconds
// function milliToSeconds(milli) {
//   let seconds = ((milli % 60000) / 1000).toFixed(0);
//   return seconds + ' secs';
// }

$("footer .footer-hide").click(function(){
  $(this).closest(".col-xl-2").toggleClass('open');
  $('.footlinks').toggleClass("open");  
});
// $(".open").click(function(){
//   $(this).toggleClass("hide");
//   $('.footlinks').toggleClass("hide");  
// });


//smooth

// let Scrollbar = window.Scrollbar;

// const options = {
//   'damping' : .01
// }

// Scrollbar.init(document.querySelector('body'), options);



// know about section li dropdown in mobile





// $(window).on('load', function() {
//   if ($(window).width() < 991) {
//     $(".tab-list").on("click", ".init", function() {
//       $(this).closest(".tab-list").children('li:not(.init)').toggle();
//       // console.log();
//     });
    
//     var allOptions = $(".tab-list").children('li:not(.init)');
//     $(".tab-list").on("click", "li:not(.init)", function() {
//       allOptions.removeClass('selected');
//       $(this).addClass('selected');
//       $(".tab-list").children('.init').html($(this).html());
//       allOptions.toggle();
//     });
//   }
// });

// Animation

// var controller = new ScrollMagic.Controller();

function fadeinup(x,y){
  var anim_elem = x;
  var trig_elem = y;
  var fadeinup1 = TweenMax.staggerFromTo(anim_elem, 0.8, {y:20}, {y:0,opacity:1,force3D:true}, 0.2);
  // new ScrollMagic.Scene({triggerElement: trig_elem,reverse: false,triggerHook: 'onEnter', offset: 250}).setTween(fadeinup1).addTo(controller);
}

var h_home = $('.top-banner');
var h_service = $('.service_sec');

if(h_home.length !=0){
  // gsap.fromTo('.banner-text-inner h1, .banner-text-inner span, .banner-text-inner button', 0.8, {x:100,opacity:0}, {x:0,opacity:1,delay:.4},0.2);
  gsap.fromTo('.banner-text-inner h1', 0.7, {y:100, opacity:0}, {y:0, opacity:1, delay:.3},0.5);
  gsap.fromTo('.banner-text-inner span', 0.7, {y:100, opacity: 0}, {y:0, opacity: 1, delay:.5},0.5);
  gsap.fromTo('.banner-text-inner .buttin-defult', 0.7, {y:100, opacity: 0}, {y:0, opacity: 1, delay:.7},0.5);
}

// gsap.fromTo('.banner-text-inner h1', 0.7, {y:100, opacity:0}, {y:0, opacity:1, delay:.3},0.5);
// gsap.fromTo('.banner-text-inner span', 0.7, {y:100, opacity: 0}, {y:0, opacity: 1, delay:.5},0.5);
// gsap.fromTo('.banner-text-inner .banner-btn', 0.7, {y:100, opacity: 0}, {y:0, opacity: 1, delay:.7},0.5);

// gsap.fromTo('.banner-bottom-nav span', 0.8, {x:'-100vw', opacity: 0}, {x:'0', opacity: 1, delay:.9},0.10);
// AOS.init();

$(document).scroll(function() {
  var y = $(this).scrollTop();
  if (y > 100) {
    $('.sticky-buttons').fadeIn();
  } else {
   $('.sticky-buttons').fadeOut();
  }
});

$(".featured-gallery").hover(
  function () {
    $(this).parents('section').addClass("featured_gallery_hover");
  },
  function () {
    $(this).parents('section').removeClass("featured_gallery_hover");
  }
);

$('.featured-gallery').parents('section').addClass('first_one');
$('.owl-carousel:not(.years-carousel)').on('changed.owl.carousel initialized.owl.carousel', function (event) {
    $(event.target)
        .find('.owl-item').removeClass('end')
        .eq(event.item.index).addClass('end');
})
$(".custom-owl-right-arow").click(function(){
  if ($(this).closest('section').find('.owl-stage .owl-item:last-of-type').hasClass('end')){
    $(this).parents('section').addClass('last_one');
  } else {
    $(this).parents('section').removeClass('last_one');
  }
  $(this).parents('section').removeClass('first_one ');
});
$(".custom-owl-left-arow").click(function(){
  if ($(this).closest('section').find('.owl-stage .owl-item:first-of-type').hasClass('end')){
    $(this).parents('section').addClass('first_one ');
  } else {
    $(this).parents('section').removeClass('first_one ');
  }
  $(this).parents('section').removeClass('last_one');
});

var videoSlider = $('.banner_slider');
videoSlider.owlCarousel({
  autoplay: true,
  lazyLoad: false,
  loop: true,      
  items:1,         
  autoplayTimeout: 9000,
  smartSpeed: 800,
  nav: false,  
});
// var videoSlider = $('.banner_slider');
// videoSlider.on('translate.owl.carousel', function (e) {
//   $('.owl-item .video-holder .videosss').each(function () {
//     // pause playing video - after sliding
//     player11.pause();
//   });
// });
// videoSlider.on('translated.owl.carousel', function (e) {
//   if ($('.owl-item.active').find('.video-holder').length !== 0) {
//     player11.play();
//   }
// });
$('.custom-owl-right-arow').click(function() {
  $(this).parents('section').find(".banner_slider").trigger('next.owl.carousel');
});
$('.custom-owl-left-arow').click(function() {
  $(this).parents('section').find(".banner_slider").trigger('prev.owl.carousel');
});