$(document).ready(function () {
  $("#news_slider").slick({
    infinite: true,
    slidesToShow: 1,
    rows: 5,
    slidesToScroll: 1,
    prevArrow: `<button class="slick_arrow slick_prev"><svg xmlns="http://www.w3.org/2000/svg" width="7.023" height="14.017" viewBox="0 0 7.023 14.017">
      <path id="arow" d="M-.161.133A.5.5,0,0,0-.8.1L-.867.161-6.89,6.678a.5.5,0,0,0-.058.6l.058.076,6.023,6.5a.5.5,0,0,0,.707.027.5.5,0,0,0,.082-.635l-.055-.071L-5.389,7.5a.766.766,0,0,1-.268-.5.81.81,0,0,1,.284-.5L-.133.839A.5.5,0,0,0-.1.2Z" transform="translate(7.023)" fill="#84754e"></path>
    </svg></button>`,
    nextArrow: `<button class="slick_arrow slick_next"><svg xmlns="http://www.w3.org/2000/svg" width="7.023" height="14.017" viewBox="0 0 7.023 14.017">
      <path id="arow" d="M.161.133A.5.5,0,0,1,.8.1L.867.161,6.89,6.678a.5.5,0,0,1,.058.6l-.058.076-6.023,6.5a.5.5,0,0,1-.789-.608l.055-.071L5.389,7.5a.766.766,0,0,0,.268-.5.81.81,0,0,0-.284-.5L.133.839A.5.5,0,0,1,.1.2Z" fill="#84754e"></path>
    </svg></button>`,
  });

  var selectbox = document.querySelectorAll(".expertise-selectbox");
  selectbox.forEach((select) => {
    select.addEventListener("click", () => {
      select.classList.toggle("selectopen");
    });
    select.querySelectorAll(".dropdown-options a").forEach((a) => {
      a.addEventListener("click", () => {
        select.querySelector("span").innerText = a.innerText;
      });
    });
    select.querySelectorAll(".dropdown-options li").forEach((a) => {
      a.addEventListener("click", () => {
        select.querySelector("span").innerText = a.innerText;
      });
    });
  });

  // know about link slider
  $(".links-slider").slick({
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    variableWidth: true,
    draggable: true,
    swipe: true,
    touchMove: true,
    prevArrow: `<button class="slick_arrow slick_prev"><svg xmlns="http://www.w3.org/2000/svg" width="15" height="20" viewBox="0 0 7.023 14.017">
  <path id="arow" d="M-.161.133A.5.5,0,0,0-.8.1L-.867.161-6.89,6.678a.5.5,0,0,0-.058.6l.058.076,6.023,6.5a.5.5,0,0,0,.707.027.5.5,0,0,0,.082-.635l-.055-.071L-5.389,7.5a.766.766,0,0,1-.268-.5.81.81,0,0,1,.284-.5L-.133.839A.5.5,0,0,0-.1.2Z" transform="translate(7.023)" fill="#84754e"></path>
  </svg></button>`,
    nextArrow: `<button class="slick_arrow slick_next"><svg xmlns="http://www.w3.org/2000/svg" width="15" height="20" viewBox="0 0 7.023 14.017">
  <path id="arow" d="M.161.133A.5.5,0,0,1,.8.1L.867.161,6.89,6.678a.5.5,0,0,1,.058.6l-.058.076-6.023,6.5a.5.5,0,0,1-.789-.608l.055-.071L5.389,7.5a.766.766,0,0,0,.268-.5.81.81,0,0,0-.284-.5L.133.839A.5.5,0,0,1,.1.2Z" fill="#84754e"></path>
  </svg></button>`,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 500,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  });

  $(".latest_vacancies").slick({
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 2,
    rows: 1,
    variableWidth: false,
    draggable: true,
    swipe: true,
    touchMove: true,
    prevArrow: `<button class="slick_arrow slick_prev"><svg xmlns="http://www.w3.org/2000/svg" width="15" height="20" viewBox="0 0 7.023 14.017">
  <path id="arow" d="M-.161.133A.5.5,0,0,0-.8.1L-.867.161-6.89,6.678a.5.5,0,0,0-.058.6l.058.076,6.023,6.5a.5.5,0,0,0,.707.027.5.5,0,0,0,.082-.635l-.055-.071L-5.389,7.5a.766.766,0,0,1-.268-.5.81.81,0,0,1,.284-.5L-.133.839A.5.5,0,0,0-.1.2Z" transform="translate(7.023)" fill="#84754e"></path>
  </svg></button>`,
    nextArrow: `<button class="slick_arrow slick_next"><svg xmlns="http://www.w3.org/2000/svg" width="15" height="20" viewBox="0 0 7.023 14.017">
  <path id="arow" d="M.161.133A.5.5,0,0,1,.8.1L.867.161,6.89,6.678a.5.5,0,0,1,.058.6l-.058.076-6.023,6.5a.5.5,0,0,1-.789-.608l.055-.071L5.389,7.5a.766.766,0,0,0,.268-.5.81.81,0,0,0-.284-.5L.133.839A.5.5,0,0,1,.1.2Z" fill="#84754e"></path>
  </svg></button>`,
    responsive: [
      {
        breakpoint: 570,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  });

  $(".expertise_slider_block").slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    variableWidth: false,
    draggable: true,
    swipe: true,
    touchMove: true,
    // prevArrow:$('.btn_inner_wrap .slick_arrow.slick_prev'),
    // nextArrow:$('.btn_inner_wrap .slick_arrow.slick_next')
  });
  $(".btn_inner_wrap .slick_arrow.slick_prev").click(function () {
    $(this).closest(".expertise_slider_block").slick("slickPrev");
  });
  $(".btn_inner_wrap .slick_arrow.slick_next").click(function () {
    $(this).closest(".expertise_slider_block").slick("slickNext");
  });

  $(".expertise_logo_list").slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    variableWidth: false,
    draggable: true,
    swipe: true,
    //touchMove: true,
    appendArrows: $(this)
      .closest(".body_copy")
      .find(".btn_inner_wrap .slider_btn"),
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
        },
      },
    ],
    prevArrow: `<button class="slick_arrow slick_prev">
  <svg xmlns="http://www.w3.org/2000/svg" width="15" height="20" viewBox="0 0 7.023 14.017">
    <path id="arow" d="M-.161.133A.5.5,0,0,0-.8.1L-.867.161-6.89,6.678a.5.5,0,0,0-.058.6l.058.076,6.023,6.5a.5.5,0,0,0,.707.027.5.5,0,0,0,.082-.635l-.055-.071L-5.389,7.5a.766.766,0,0,1-.268-.5.81.81,0,0,1,.284-.5L-.133.839A.5.5,0,0,0-.1.2Z" transform="translate(7.023)" fill="#84754e"></path>
  </svg>
</button>`,
    nextArrow: `<button class="slick_arrow slick_next">
  <svg xmlns="http://www.w3.org/2000/svg" width="15" height="20" viewBox="0 0 7.023 14.017">
    <path id="arow" d="M.161.133A.5.5,0,0,1,.8.1L.867.161,6.89,6.678a.5.5,0,0,1,.058.6l-.058.076-6.023,6.5a.5.5,0,0,1-.789-.608l.055-.071L5.389,7.5a.766.766,0,0,0,.268-.5.81.81,0,0,0-.284-.5L.133.839A.5.5,0,0,1,.1.2Z" fill="#84754e"></path>
  </svg>
</button>`,
  });
  $(".btn_inner_wrap .slick_arrow.slick_prev").click(function () {
    $(this)
      .closest(".body_copy")
      .find(".expertise_logo_list")
      .slick("slickPrev");
  });
  $(".btn_inner_wrap .slick_arrow.slick_next").click(function () {
    $(this)
      .closest(".body_copy")
      .find(".expertise_logo_list")
      .slick("slickNext");
  });

  //   $(".mobile_decades_slider .decade_wrapper").slick({
  //     infinite: false,
  //     slidesToShow: 1,
  //     slidesToScroll: 1,
  //     variableWidth: false,
  //     draggable: true,
  //     swipe: true,
  //     touchMove: true,
  //     speed: 200,
  //     responsive: [
  //       {
  //         breakpoint: 1024,
  //         settings: {
  //           useTransform: false,
  //         },
  //       },
  //     ],
  //     prevArrow: `<div class="mobile_timeline-slick slick_prev">
  //     <svg xmlns="http://www.w3.org/2000/svg" width="15" height="20" viewBox="0 0 7.023 14.017">
  //     <path id="arow" d="M-.161.133A.5.5,0,0,0-.8.1L-.867.161-6.89,6.678a.5.5,0,0,0-.058.6l.058.076,6.023,6.5a.5.5,0,0,0,.707.027.5.5,0,0,0,.082-.635l-.055-.071L-5.389,7.5a.766.766,0,0,1-.268-.5.81.81,0,0,1,.284-.5L-.133.839A.5.5,0,0,0-.1.2Z" transform="translate(7.023)" fill="#84754e"></path>
  //   </svg>
  // </div>`,
  //     nextArrow: `<div class="mobile_timeline-slick slick_next">
  //   <svg xmlns="http://www.w3.org/2000/svg" width="15" height="20" viewBox="0 0 7.023 14.017">
  //     <path id="arow" d="M.161.133A.5.5,0,0,1,.8.1L.867.161,6.89,6.678a.5.5,0,0,1,.058.6l-.058.076-6.023,6.5a.5.5,0,0,1-.789-.608l.055-.071L5.389,7.5a.766.766,0,0,0,.268-.5.81.81,0,0,0-.284-.5L.133.839A.5.5,0,0,1,.1.2Z" fill="#84754e"></path>
  //   </svg>
  // </div>`,
  //   });
  $(".scroll_icon").click(function () {
    var next = $(this).closest(".decade_slide").next(".decade_slide");
    $(next).addClass("next_scroll");
    var target = document.querySelector(".next_scroll");
    target.scrollIntoView({
      behavior: "smooth",
      block: "start",
      inline: "nearest",
    });
    setTimeout(() => {
      $(".next_scroll").removeClass("next_scroll");
    }, 2000);
  });

  $(".decade_navigation .custom_selectbox button").click(function () {
    $(".decade_navigation .custom_selectbox ").toggleClass("nav_open");
  });
  $(".decade_navigation .custom_selectbox ul li a").click(function () {
    $(".decade_navigation .custom_selectbox ").removeClass("nav_open");
  });

  $(document).on("click", function (event) {
    var $trigger = $(".custom_selectbox");
    if ($trigger !== event.target && !$trigger.has(event.target).length) {
      $(".decade_navigation .custom_selectbox ").removeClass("nav_open");
    }
  });

  $(".home-expertise-list").slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    variableWidth: false,
    draggable: true,
    swipe: true,
    //touchMove: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
        },
      },
    ],
    // prevArrow:$('.btn_inner_wrap .slick_arrow.slick_prev'),
    // nextArrow:$('.btn_inner_wrap .slick_arrow.slick_next')
  });
  $(".gallery-content .slider .exp-prev").click(function () {
    $(this)
      .closest(".expertise")
      .find(".home-expertise-list")
      .slick("slickPrev");
  });
  $(".gallery-content .slider .exp-next").click(function () {
    $(this)
      .closest(".expertise")
      .find(".home-expertise-list")
      .slick("slickNext");
  });

  $(".facts-carousal").slick({
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    variableWidth: false,
    draggable: true,
    swipe: true,
    autoplay: true,
    autoplaySpeed: 1500,
    //touchMove: true,
    // cssEase:"linear",
    speed: 1700,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
    // prevArrow:$('.btn_inner_wrap .slick_arrow.slick_prev'),
    // nextArrow:$('.btn_inner_wrap .slick_arrow.slick_next')
  });
  $(".facts-desc-box .slider .dream-right").click(function () {
    $(this)
      .parents(".facts-desc-box")
      .siblings(".facts-list-box")
      .find(".facts-carousal")
      .slick("slickPrev");
  });
  $(".facts-desc-box .slider .dream-left").click(function () {
    $(this)
      .parents(".facts-desc-box")
      .siblings(".facts-list-box")
      .find(".facts-carousal")
      .slick("slickNext");
  });
});

$(".video_player_popup_btn").click(function () {
  $(this)
    .closest(".container")
    .find(".video_player_popup_window")
    .addClass("show_window");
  $("body").addClass("overflow_hidden");
});
$(".close_popup_btn").click(function () {
  $(this).closest(".video_player_popup_window").removeClass("show_window");
  $("body").removeClass("overflow_hidden");
});

$(".decade_navigation li a").click(function (e) {
  e.preventDefault();
  var scrollElm = document.getElementById(
    `${$(this).attr("href").replace("#", "")}`
  );
  let top = scrollElm.offsetTop;
  window.scrollTo({
    top: top - 68,
    behavior: "smooth",
  });
});
