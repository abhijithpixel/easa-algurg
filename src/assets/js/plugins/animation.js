/*!
 * VERSION: 1.7.5
 * DATE: 2015-02-26
 * UPDATES AND DOCS AT: http://greensock.com
 *
 * @license Copyright (c) 2008-2015, GreenSock. All rights reserved.
 * This work is subject to the terms at http://greensock.com/standard-license or for
 * Club GreenSock members, the software agreement that was issued with your membership.
 *
 * @author: Jack Doyle, jack@greensock.com
 **/
function tweenmax_animation(){
var _gsScope="undefined"!=typeof module&&module.exports&&"undefined"!=typeof global?global:this||window;(_gsScope._gsQueue||(_gsScope._gsQueue=[])).push(function(){"use strict";var t=document.documentElement,e=window,i=function(i,r){var s="x"===r?"Width":"Height",n="scroll"+s,a="client"+s,o=document.body;return i===e||i===t||i===o?Math.max(t[n],o[n])-(e["inner"+s]||t[a]||o[a]):i[n]-i["offset"+s]},r=_gsScope._gsDefine.plugin({propName:"scrollTo",API:2,version:"1.7.5",init:function(t,r,s){return this._wdw=t===e,this._target=t,this._tween=s,"object"!=typeof r&&(r={y:r}),this.vars=r,this._autoKill=r.autoKill!==!1,this.x=this.xPrev=this.getX(),this.y=this.yPrev=this.getY(),null!=r.x?(this._addTween(this,"x",this.x,"max"===r.x?i(t,"x"):r.x,"scrollTo_x",!0),this._overwriteProps.push("scrollTo_x")):this.skipX=!0,null!=r.y?(this._addTween(this,"y",this.y,"max"===r.y?i(t,"y"):r.y,"scrollTo_y",!0),this._overwriteProps.push("scrollTo_y")):this.skipY=!0,!0},set:function(t){this._super.setRatio.call(this,t);var r=this._wdw||!this.skipX?this.getX():this.xPrev,s=this._wdw||!this.skipY?this.getY():this.yPrev,n=s-this.yPrev,a=r-this.xPrev;this._autoKill&&(!this.skipX&&(a>7||-7>a)&&i(this._target,"x")>r&&(this.skipX=!0),!this.skipY&&(n>7||-7>n)&&i(this._target,"y")>s&&(this.skipY=!0),this.skipX&&this.skipY&&(this._tween.kill(),this.vars.onAutoKill&&this.vars.onAutoKill.apply(this.vars.onAutoKillScope||this._tween,this.vars.onAutoKillParams||[]))),this._wdw?e.scrollTo(this.skipX?r:this.x,this.skipY?s:this.y):(this.skipY||(this._target.scrollTop=this.y),this.skipX||(this._target.scrollLeft=this.x)),this.xPrev=this.x,this.yPrev=this.y}}),s=r.prototype;r.max=i,s.getX=function(){return this._wdw?null!=e.pageXOffset?e.pageXOffset:null!=t.scrollLeft?t.scrollLeft:document.body.scrollLeft:this._target.scrollLeft},s.getY=function(){return this._wdw?null!=e.pageYOffset?e.pageYOffset:null!=t.scrollTop?t.scrollTop:document.body.scrollTop:this._target.scrollTop},s._kill=function(t){return t.scrollTo_x&&(this.skipX=!0),t.scrollTo_y&&(this.skipY=!0),this._super._kill.call(this,t)}}),_gsScope._gsDefine&&_gsScope._gsQueue.pop()();
 
var controller = new ScrollMagic.Controller();

function fadeinup(x,y){
    var anim_elem = x;
    var trig_elem = y;
    var fadeinup1 = TweenMax.staggerFromTo(anim_elem, 0.8, {y:20}, {y:0,opacity:1,force3D:true}, 0.2);
    new ScrollMagic.Scene({triggerElement: trig_elem,reverse: false,triggerHook: 'onEnter', offset: 250}).setTween(fadeinup1).addTo(controller);
}




/*  home page animations */
var h_home = $('.top-banner');
var h_service = $('.service_sec');
var h_logistic = $('.logistic_sec');
var h_get_start = $('.get_start_sec');
var h_whatsnew = $('.whats_new_sec');
var geagraphic_home = $('.geagraphic-home');
var technology_home = $('.technology-home');
var growth_home = $('.growth-home');
 
if(h_home.length !=0){
    TweenMax.staggerFromTo('.banner-text-inner h1, .banner-text-inner span, .banner-text-inner .banner-btn', 0.5, {y:30,opacity:0}, {y:0,opacity:1,delay:.3},0.2);
    // TweenMax.staggerFromTo('.counter_inner', 0.6, {y:30,opacity:0}, {y:0,opacity:1,delay:.7},0.1);
}
if(h_service.length !=0){
    var service_sec = new TimelineLite(); 
    var service_sec1 = service_sec.staggerFromTo('.service_sec .service_item:first-child, .text_div .small_head, .text_div h2, .service_sec .service_item:not(:first-child)', 0.6, {y:30}, {y:0,opacity:1},0.2)
    new ScrollMagic.Scene({triggerElement: '.service_sec',reverse: false,triggerHook: 'onEnter',offset: 250}).setTween(service_sec1).addTo(controller);
}
if(geagraphic_home.length !=0){
    var geagraphic_home = new TimelineLite(); 
    var geagraphic_home1 = geagraphic_home.staggerFromTo('.geagraphic-home .americas-img, .geagraphic-home article', 0.6, {y:30}, {y:0,opacity:1},0.2) 
    new ScrollMagic.Scene({triggerElement: '.geagraphic-home',reverse: false,triggerHook: 'onEnter',offset: 250}).setTween(geagraphic_home1).addTo(controller);
}
if(technology_home.length !=0){
    var technology_home = new TimelineLite(); 
    var technology_home1 = technology_home.staggerFromTo('.technology-home .americas-img, .technology-home article', 0.6, {y:30}, {y:0,opacity:1},0.2) 
    new ScrollMagic.Scene({triggerElement: '.technology-home',reverse: false,triggerHook: 'onEnter',offset: 250}).setTween(technology_home1).addTo(controller);
}
if(growth_home.length !=0){
    var growth_home = new TimelineLite(); 
    var growth_home1 = growth_home.staggerFromTo('.growth-home .americas-img, .growth-home article', 0.6, {y:30}, {y:0,opacity:1},0.2) 
    new ScrollMagic.Scene({triggerElement: '.growth-home',reverse: false,triggerHook: 'onEnter',offset: 250}).setTween(growth_home1).addTo(controller);
}
if(h_logistic.length !=0){
    var logistic_sec = new TimelineLite(); 
    var logistic_sec1 = logistic_sec.staggerFromTo('.logistiv_div .logo_svg, .logistiv_div h4, .logistiv_div p', 0.5, {y:30, opacity:0}, {y:0,opacity:1},0.1)
    .staggerFromTo('.logistic_sec .logistic_item', 0.5, {y:30, opacity:0}, {y:0,opacity:1},0.2)
    new ScrollMagic.Scene({triggerElement: '.logistic_sec',reverse: false,triggerHook: 'onEnter',offset: 250}).setTween(logistic_sec1).addTo(controller);
}
if(h_get_start.length !=0){
    var get_start_sec = new TimelineLite(); 
    var get_start_sec1 = get_start_sec.staggerFromTo('.get_start_sec .get_start_content, .get_start_sec .get_start_content h2, .get_start_sec .get_start_content p', 0.5, {y:30, opacity:0}, {y:0,opacity:1},0.3)
    new ScrollMagic.Scene({triggerElement: '.get_start_sec',reverse: false,triggerHook: 'onEnter',offset: 250}).setTween(get_start_sec1).addTo(controller);
}
if(h_whatsnew.length !=0){
    var whats_new_sec = new TimelineLite(); 
    var whats_new_sec1 = whats_new_sec.staggerFromTo('.whats_new_sec .whats_head, .whats_new_sec .row_div .col_div', 0.5, {y:30, opacity:0}, {y:0,opacity:1},0.3)
    new ScrollMagic.Scene({triggerElement: '.whats_new_sec',reverse: false,triggerHook: 'onEnter',offset: 250}).setTween(whats_new_sec1).addTo(controller);
}


/** About page Animation **/

var ab_banner = $('.inner-banner');
var ab_lsi = $('.about-lsi');
var ab_why_lsi = $('.why-lsi');
var ab_why_lsi_botm = $('.why-lsi-bottom-part');
var ab_our_vition = $('.our-vision');
var ab_bordof_directors = $('.board-of-directors');
var ab_managment = $('.management-team');

if(ab_banner.length !=0){
    TweenMax.staggerFromTo('.inner-banner .col h1, .inner-banner .col', 0.6, {y:30,opacity:0}, {y:0,opacity:1},0.1);
}
if(ab_lsi.length !=0){
    TweenMax.staggerFromTo('.about-lsi .col-left .content h5, .about-lsi .col-left .content h1, .about-lsi .col-right p', 0.6, {y:30,opacity:0}, {y:0,opacity:1},0.1);
}
if(ab_why_lsi.length !=0){
    var ab_why_lsi_sec = new TimelineLite(); 
    var ab_why_lsi_sec1 = ab_why_lsi_sec.staggerFromTo('.why-lsi .col-left .img-holder', 0.5, {y:30, opacity:0}, {y:0,opacity:1},0.1)
    .staggerFromTo('.why-lsi .col-right .content h3, .why-lsi .col-right .content h5, .why-lsi .col-right .content p', 0.5, {y:30, opacity:0}, {y:0,opacity:1},0.2)
    new ScrollMagic.Scene({triggerElement: ab_why_lsi,reverse: false,triggerHook: 'onEnter',offset: 0}).setTween(ab_why_lsi_sec1).addTo(controller);
}
if(ab_why_lsi_botm.length !=0){
    var ab_why_lsi_botm_sec = new TimelineLite(); 
    var ab_why_lsi_botm_sec1 = ab_why_lsi_botm_sec.staggerFromTo('.why-lsi-bottom-part .col-four', 0.5, {y:30, opacity:0}, {y:0,opacity:1},0.2)
    new ScrollMagic.Scene({triggerElement: '.why-lsi-bottom-part',reverse: false,triggerHook: 'onEnter',offset: 250}).setTween(ab_why_lsi_botm_sec1).addTo(controller);
}
if(ab_our_vition.length !=0){
    var ab_our_vition_sec = new TimelineLite(); 
    var ab_our_vition_sec1 = ab_our_vition_sec.staggerFromTo('.our-vision .col-left .content h3, .our-vision .col-left .content h5, .our-vision .col-left .content p, .our-vision .col-right .img-fluid', 0.5, {y:30, opacity:0}, {y:0,opacity:1},0.2)
    new ScrollMagic.Scene({triggerElement: '.our-vision',reverse: false,triggerHook: 'onEnter',offset: 250}).setTween(ab_our_vition_sec1).addTo(controller);
}
if(ab_bordof_directors.length !=0){
    var ab_bordof_directors_sec = new TimelineLite(); 
    var ab_bordof_directors_sec1 = ab_bordof_directors_sec.staggerFromTo('.board-of-directors h3, .board_of_director_slider .item>div', 0.5, {y:30, opacity:0}, {y:0,opacity:1},0.3)
    new ScrollMagic.Scene({triggerElement: '.board-of-directors',reverse: false,triggerHook: 'onEnter',offset: 250}).setTween(ab_bordof_directors_sec1).addTo(controller);
}
if(ab_managment.length !=0){
    var ab_managment_sec = new TimelineLite(); 
    var ab_managment_sec1 = ab_managment_sec.staggerFromTo('.management-team h3, .management_team_slider .owl-stage .owl-item .item>div', 0.5, {y:30, opacity:0}, {y:0,opacity:1},0.2)
    new ScrollMagic.Scene({triggerElement: '.management-team',reverse: false,triggerHook: 'onEnter',offset: 250}).setTween(ab_managment_sec1).addTo(controller);
}

/** contact page **/
var banner_head = $('.banner_head');
var cnct_our_office = $('.our_office');
var our_office_item = $('.our_office .sub_head, .our_office h2, .our_office .country_tab ul, .our_office .country_tab_detail, .our_office .country_tab_detail .detail_item');
var cnct_happy_help = $('.happy_help');
var cnct_map = $('.map_sec');

if(banner_head.length !=0){
    TweenMax.staggerFromTo('.banner_head>h1', 0.6, {y:30,opacity:0}, {y:0,opacity:1},0.1);
}
if(cnct_our_office.length !=0){
    TweenMax.staggerFromTo(our_office_item, 0.6, {y:30,opacity:0}, {y:0,opacity:1},0.2);
}
if(cnct_happy_help.length !=0){
    var cnct_happy_help_sec = new TimelineLite(); 
    var cnct_happy_help_sec1 = cnct_happy_help_sec.staggerFromTo('.contact_col h2, .contact_col p, .contact_col .contact_form .form_col, .happy_help .image_col', 0.5, {y:30, opacity:0}, {y:0,opacity:1},0.2)
    new ScrollMagic.Scene({triggerElement: '.happy_help',reverse: false,triggerHook: 'onEnter',offset: 250}).setTween(cnct_happy_help_sec1).addTo(controller);
}
if(cnct_map.length !=0){
    var cnct_map_sec = new TimelineLite(); 
    var cnct_map_sec1 = cnct_map_sec.staggerFromTo('.map_sec .sub_head, .map_sec h2, .map_sec .map_div', 0.5, {y:30, opacity:0}, {y:0,opacity:1},0.2)
    new ScrollMagic.Scene({triggerElement: '.map_sec',reverse: false,triggerHook: 'onEnter',offset: 250}).setTween(cnct_map_sec1).addTo(controller);
}

/** group companies **/
var lsi_servce = $('.lsi_service_sec');
var help_you = $('.help_you');

if(lsi_servce.length !=0){
    if ($(window).width() > 991) {
        TweenMax.staggerFromTo('.lsi_service_sec .side_bar ul li', 0.6, {x:30,opacity:0}, {x:0,opacity:1},0.2);
    }
    TweenMax.staggerFromTo('.main_discription .detail_img .img, .lsi_service_sec .main_discription .detail_div *', 0.6, {y:30,opacity:0}, {y:0,opacity:1},0.2);

    var service_blue = new TimelineLite(); 
    var service_blue1 = service_blue.staggerFromTo('.bluebg_div .div_item', 0.6, {y:30, opacity:0}, {y:0,opacity:1},0.3)
    new ScrollMagic.Scene({triggerElement: '.bluebg_div',reverse: false,triggerHook: 'onEnter',offset: 250}).setTween(service_blue1).addTo(controller);    
    var service_blue = new TimelineLite(); 
    var service_blue1 = service_blue.staggerFromTo('.lsi_service_sec .service_detail .main_discription+.content_only_div', 0.6, {y:30, opacity:0}, {y:0,opacity:1},0.3)
    new ScrollMagic.Scene({triggerElement: '.content_only_div',reverse: false,triggerHook: 'onEnter',offset: 250}).setTween(service_blue1).addTo(controller);    
    
    var service_blue = new TimelineLite(); 
    var service_blue1 = service_blue.staggerFromTo('.offer_tab .tab_head .sub_head, .offer_tab .tab_head h3, .tab_head .tab_div ul>li, .offer_tab .tab_detail>div', 0.5, {y:30, opacity:0}, {y:0,opacity:1},0.2)
    new ScrollMagic.Scene({triggerElement: '.offer_tab',reverse: false,triggerHook: 'onEnter',offset: 250}).setTween(service_blue1).addTo(controller);    
   
    var service_blue = new TimelineLite(); 
    var service_blue1 = service_blue.staggerFromTo('.requirement h3, .require_row .logos .logo_image', 0.5, {y:30, opacity:0}, {y:0,opacity:1},0.2)
    new ScrollMagic.Scene({triggerElement: '.requirement',reverse: false,triggerHook: 'onEnter',offset: 250}).setTween(service_blue1).addTo(controller);    
    var service_blue = new TimelineLite(); 
    var service_blue1 = service_blue.staggerFromTo('.require_row .requirement_div ul>li', 0.5, {y:30, opacity:0}, {y:0,opacity:1},0.2)
    new ScrollMagic.Scene({triggerElement: '.requirement',reverse: false,triggerHook: 'onEnter',offset: 250}).setTween(service_blue1).addTo(controller);    
    
    var countre_div = new TimelineLite(); 
    var countre_div1 = countre_div.staggerFromTo('.counter_number_div .item_countdiv', 0.5, {y:30, opacity:0}, {y:0,opacity:1},0.2)
    new ScrollMagic.Scene({triggerElement: '.counter_number_div',reverse: false,triggerHook: 'onEnter',offset: 250}).setTween(countre_div1).addTo(controller);    
    
    var what_we_do = new TimelineLite(); 
    var what_we_do1 = what_we_do.staggerFromTo('.what_we_do .sub_head, .what_we_do h3, .what_we_do .wedo_row .wedo_item, .what_we_do .wedo_full_row', 0.5, {y:30, opacity:0}, {y:0,opacity:1},0.2)
    new ScrollMagic.Scene({triggerElement: '.what_we_do',reverse: false,triggerHook: 'onEnter',offset: 250}).setTween(what_we_do1).addTo(controller);    
}
if(lsi_servce.length !=0){
    var help_you = new TimelineLite(); 
    var help_you1 = help_you.staggerFromTo('.help_you .sub_head, .help_you h3, .help_you p', 0.5, {y:30, opacity:0}, {y:0,opacity:1},0.2)
    new ScrollMagic.Scene({triggerElement: '.help_you',reverse: false,triggerHook: 'onEnter',offset: 250}).setTween(help_you1).addTo(controller);    
}


/*** career page ***/
var career_join_sec = $('.join_us_sec');
var career_we_hiring = $('.we_hiring');
var career_why_work = $('.why_work');
var career_our_values = $('.our_values');
var career_job_board = $('.job_board');

if(career_join_sec.length !=0){
    TweenMax.staggerFromTo('.join_us_sec .join_img, .join_us_sec .join_content .sub_head, .join_us_sec .join_content h2, .join_us_sec .join_content p', 0.6, {y:30,opacity:0}, {y:0,opacity:1,delay:0.2},0.2);
}
if(career_we_hiring.length !=0){
    var we_hiring = new TimelineLite(); 
    var we_hiring1 = we_hiring.staggerFromTo('.we_hiring .head_div h2, .we_hiring .filter_option>div, .we_hiring .career_opning>div', 0.5, {y:30, opacity:0}, {y:0,opacity:1},0.1)
    new ScrollMagic.Scene({triggerElement: '.we_hiring',reverse: false,triggerHook: 'onEnter',offset: 250}).setTween(we_hiring1).addTo(controller);    
}
if(career_why_work.length !=0){
    var why_work = new TimelineLite(); 
    var why_work1 = why_work.staggerFromTo('.why_work .sec_head, .why_work .p_head, .why_work .col_div .col_item, .why_work .col_img .img', 0.5, {y:30, opacity:0}, {y:0,opacity:1},0.1)
    new ScrollMagic.Scene({triggerElement: '.why_work',reverse: false,triggerHook: 'onEnter',offset: 250}).setTween(why_work1).addTo(controller);    
}
if(career_our_values.length !=0){
    var our_values = new TimelineLite(); 
    var our_values1 = our_values.staggerFromTo('.our_values .sec_head, .our_values .p_head, .our_values .col_item', 0.5, {y:30, opacity:0}, {y:0,opacity:1},0.1)
    new ScrollMagic.Scene({triggerElement: '.our_values',reverse: false,triggerHook: 'onEnter',offset: 250}).setTween(our_values1).addTo(controller);    
}
if(career_job_board.length !=0){
    var job_board = new TimelineLite(); 
    var job_board1 = job_board.staggerFromTo('.job_board .sec_head, .job_board .p_head, .job_board aside .search, .job_board article .search_table, .job_board .search_table .search_div', 0.5, {y:30, opacity:0}, {y:0,opacity:1},0.1)
    new ScrollMagic.Scene({triggerElement: '.job_board',reverse: false,triggerHook: 'onEnter',offset: 250}).setTween(job_board1).addTo(controller);    
}























// /* mobile menu */
var mobmenu = $('.h_menu_ul>ul>li');
if ($(window).width() <= 991) {

    $('.hamburger').on('click', function() {
        if($('body').hasClass('mobile_menu')){
            TweenMax.staggerFromTo(mobmenu, 1, {x:30, opacity:0}, {x:0,opacity:1,force3D:true}, 0.2);
        } else {
            TweenMax.killTweensOf('.h_menu_ul>ul>li');
            $('.h_menu_ul>ul>li').removeAttr("style");
        }
    });
}

/** mouse hover effect **/
var lFollowX = 0,
    lFollowY = 0,
    x = 0,
    y = 0,
    friction = 1 / 30;

function moveBackground() {
    x += (lFollowX - x) * friction;
    y += (lFollowY - y) * friction;

    translate = 'translate(' + x + 'px, ' + y + 'px) scale(1.1)';

    $('.msevr').css({
        '-webit-transform': translate,
        '-moz-transform': translate,
        'transform': translate
    });

    window.requestAnimationFrame(moveBackground);
}

$(window).on('mousemove click', function (e) {

    var lMouseX = Math.max(-100, Math.min(100, $(window).width() / 2 - e.clientX));
    var lMouseY = Math.max(-100, Math.min(100, $(window).height() / 2 - e.clientY));
    lFollowX = (20 * lMouseX) / 100; // 100 : 12 = lMouxeX : lFollow
    lFollowY = (10 * lMouseY) / 100;
});
moveBackground();


/** head mask animation */
$(document).ready(function() { 
    $(".animate_elmnt").each(function() {
        var offset = $(this).offset().top;
        if ($(window).scrollTop() + $(window).height() * 0.90 >= offset) {
            $(this).addClass('trigger');
        }
    });
});

$(window).scroll(function() {
    $(".animate_elmnt").each(function() {
        var offset = $(this).offset().top;
        if ($(window).scrollTop() + $(window).height() * 0.90 >= offset) {
            $(this).addClass('trigger');
        }
    });
});




} /** function close **/